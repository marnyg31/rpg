﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPG_Characters;
using RPG_Frontend.Properties;

namespace RPG_Frontend
{
    public partial class Form1 : Form
    {
        //private readonly IDataIO _DataIO = new CharacterSaverCsv("save.csv");
        private readonly IDataIO _dataIo = new CharacterSqlIo(@"PC7581\SQLEXPRESS","RPG");
        private readonly List<Character> _allCharacters;
        private const int MaxAvailablePoints = 10;
        private Character _currentCharacter = new Warrior();

        //method to initialize state of gui
        public Form1()
        {
            InitializeComponent();
            comboBox1.Text = "select class";
            _allCharacters = _dataIo.Load();
            foreach (var character in _allCharacters) listBox2.Items.Add(character);
        }

        //method called when Submit button is clicked
        private void SubmitButton_Click(object sender, EventArgs e)
        {
            void SetCharacterStats()
            {
                _currentCharacter.HP = (int)numericUpDown1.Value;
                _currentCharacter.Energy = (int)numericUpDown2.Value;
                _currentCharacter.Armor = (int)numericUpDown3.Value;
            }
            //function to save character
            void CommitCharacter()
            {
                SetCharacterStats();
                MessageBox.Show(_currentCharacter.Summary());
                _currentCharacter.ID = _dataIo.InsertOrUpdateExisting(_currentCharacter);
                if (!listBox2.Items.Contains(_currentCharacter))
                {
                    listBox2.Items.Add(_currentCharacter);
                    _allCharacters.Add(_currentCharacter);
                }
    
                //refresh listbox
                listBox2.Items.Clear();
                foreach (Character character in _allCharacters) listBox2.Items.Add(character);
            }

            //validate and save character
            if (_currentCharacter == null || textBox1.Text == "") MessageBox.Show("select class and enter name");
            else CommitCharacter();
        }

        //set numericUpDown GUI elements to desired values
        private void SetNumericUpDownNumbers(int hp, int energy, int armour)
        {
            numericUpDown1.Value = hp;
            numericUpDown2.Value = energy;
            numericUpDown3.Value = armour;
        }

        //method called when Class of character is changed
        private void ClassTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //function to create Character object of correct type
            void CreateCharacter()
            {
                if (comboBox1.Text == "Thief") _currentCharacter = CreateCharacterOfType<Thief>(ClassType.Thief, 2, 2, 1);
                else if (comboBox1.Text == "Warrior") _currentCharacter = CreateCharacterOfType<Warrior>(ClassType.Warrior, 4, 1, 1);
                else _currentCharacter = CreateCharacterOfType<Wizard>(ClassType.Wizard, 3, 1, 2);
            }

            //function to create Character object of correct type
            T CreateCharacterOfType<T>(ClassType classType, int hp, int energy, int armour) where T : Character, new()
            {
                SetNumericUpDownNumbers(hp, energy, armour);
                return new T() { Name = textBox1.Text, ClassType = classType };
            }

            //create character object and update GUI
            CreateCharacter();
            UpdateAvailablePoints();
            UpdateImage(_currentCharacter);
        }

        //method called when stats of character is changed
        private void NumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            //function to check if you have more stats to spend
            bool StatCantChange(NumericUpDown upDown)
            {
                return numericUpDown1.Value + numericUpDown2.Value + numericUpDown3.Value > MaxAvailablePoints &
                    upDown.Value != 0;
            }

            //update stats if you still have available points
            UpdateAvailablePoints();
            NumericUpDown numericUpDown = (NumericUpDown)sender;
            if (StatCantChange(numericUpDown)) numericUpDown.Value = numericUpDown.Value - 1;
        }

        //method called when name field is changed; it updates the character name
        private void NameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (_currentCharacter != null) _currentCharacter.Name = textBox1.Text;
        }

        //method called when users selects a previously saved character
        private void SavedCharacterListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //updates the GUI
            void UpdateGuiToNewCharacter(Character character2)
            {
                textBox1.Text = character2.Name;
                comboBox1.SelectedIndexChanged -= new System.EventHandler(this.ClassTypeComboBox_SelectedIndexChanged);
                comboBox1.Text = character2.GetType().Name;
                comboBox1.SelectedIndexChanged += new System.EventHandler(this.ClassTypeComboBox_SelectedIndexChanged);

                SetNumericUpDownNumbers(character2.HP, character2.Energy, character2.Armor);
                _currentCharacter = character2;
                UpdateImage(character2);
            }

            //creates the appropriate class type for character
            Character CreateAppropriateCharacterObject()
            {
                Character character1;
                if (listBox2.SelectedItem == null) return new Warrior();
                ClassType typeOfCharacter = ((Character)listBox2.SelectedItem).ClassType;

                if (typeOfCharacter == ClassType.Warrior) character1 = (Warrior)listBox2.SelectedItem;
                else if (typeOfCharacter == ClassType.Wizard) character1 = (Wizard)listBox2.SelectedItem;
                else character1 = (Thief)listBox2.SelectedItem;
                return character1;
            }
            //create character and update GUI
            _currentCharacter = CreateAppropriateCharacterObject();
            UpdateGuiToNewCharacter(_currentCharacter);
        }

        //update image in gui based on class selection
        private void UpdateImage(Character character)
        {
            if (character.ClassType == ClassType.Wizard) { pictureBox1.Image = Resources.wiz; }
            else if (character.ClassType == ClassType.Warrior) { pictureBox1.Image = Resources.war; }
            else { pictureBox1.Image = Resources.thief; }
        }

        //update stat points in GUI
        private void UpdateAvailablePoints()
        {
            label6.Text = (MaxAvailablePoints - (numericUpDown1.Value + numericUpDown2.Value + numericUpDown3.Value)).ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _dataIo.Delete(_currentCharacter);
            listBox2.Items.Remove(_currentCharacter);
            _allCharacters.Remove(_currentCharacter); /// All((c) => c.ID == _currentCharacter.ID);
        }
    }
}
