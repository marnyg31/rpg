﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public interface IDataIO
    {
        public List<Character> Load();
        public int InsertOrUpdateExisting(Character character);
        public void Delete(Character character);
    }
}
