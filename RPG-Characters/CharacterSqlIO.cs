﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace RPG_Characters
{
    public class CharacterSqlIo : IDataIO
    {
        private SqlConnectionStringBuilder builder;

        public CharacterSqlIo(string host, string db)
        {
            this.builder = GetStringBuilder(host, db);

        }

        //function to get sqlConnectionString
        private static SqlConnectionStringBuilder GetStringBuilder(string host,string db)
        {
            SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder();
            sqlConnectionStringBuilder.DataSource = host;
            sqlConnectionStringBuilder.InitialCatalog = db;
            sqlConnectionStringBuilder.IntegratedSecurity = true;
            return sqlConnectionStringBuilder;
        }

        //method to load all characters from db
        public List<Character> Load()
        {
            Character CreateCharacterFromRes(SqlDataReader sqlDataReader)
            {
                Character character;
                int charactertype = sqlDataReader.GetInt32(5);
                if (charactertype == (int)ClassType.Thief) character = new Thief();
                else if (charactertype == (int)ClassType.Warrior) character = new Warrior();
                else character = new Wizard();

                character.ID = sqlDataReader.GetInt32(0);
                character.Energy = sqlDataReader.GetInt32(1);
                character.HP = sqlDataReader.GetInt32(2);
                character.Armor = sqlDataReader.GetInt32(3);
                character.Name = sqlDataReader.GetString(4);
                character.ClassType = (ClassType)charactertype;

                return character;
            }

            List<Character> DoQuery(SqlConnection sqlConnection)
            {
                List<Character> characters = new List<Character>();
                string sql = "select * from Characters";
                sqlConnection.Open();
                using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                using (SqlDataReader r = cmd.ExecuteReader())
                    while (r.Read())
                        characters.Add(CreateCharacterFromRes(r));
                return characters;
            }

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                try
                {
                    return DoQuery(connection);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }


        //method to insert character into db if he does not already exist, and update db if he does 
        public int InsertOrUpdateExisting(Character character)
        {
            //function to insrt into db
            int CreateCharacter(SqlConnection sqlConnection)
            {
                string sql = "insert into Characters (Energ,HP,Aurmor,name,classType) output INSERTED.ID values (@Energy,@HP,@Armour,@name,@classtype)";
                sqlConnection.Open();
                using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                {
                    cmd.Parameters.AddWithValue("@Energy", character.Energy);
                    cmd.Parameters.AddWithValue("@HP", character.HP);
                    cmd.Parameters.AddWithValue("@Armour", character.Armor);
                    cmd.Parameters.AddWithValue("@name", character.Name);
                    cmd.Parameters.AddWithValue("@classtype", character.ClassType);
                    return (int)cmd.ExecuteScalar();
                }
            }

            //function to update db
            int UpdateCharacter(SqlConnection connection)
            {
                string sql = "update Characters set Energ=@Energy,HP=@HP,Aurmor=@Armour,name=@name,classType=@classtype where ID=@ID";
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.AddWithValue("@Energy", character.Energy);
                    cmd.Parameters.AddWithValue("@HP", character.HP);
                    cmd.Parameters.AddWithValue("@Armour", character.Armor);
                    cmd.Parameters.AddWithValue("@name", character.Name);
                    cmd.Parameters.AddWithValue("@classtype", character.ClassType);
                    cmd.Parameters.AddWithValue("@ID", character.ID);
                    cmd.ExecuteNonQuery();
                    return character.ID;
                }
            }

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                int newCharID = -1;
                try
                {
                    if (character.ID != 0) newCharID = UpdateCharacter(connection);
                    else newCharID = CreateCharacter(connection);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

                return newCharID;
            }
        }



        //method to delete character from db
        public void Delete(Character character)
        {
            void DoQuery(SqlConnection sqlConnection)
            {
                string sql = "delete from characters where ID=@ID";
                sqlConnection.Open();
                using (SqlCommand cmd = new SqlCommand(sql, sqlConnection))
                {
                    cmd.Parameters.AddWithValue("@ID", character.ID);
                    cmd.ExecuteNonQuery();
                }
            }

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                try
                {
                    DoQuery(connection);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
    }
}
