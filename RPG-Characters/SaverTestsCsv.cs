﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using Xunit;

namespace RPG_Characters
{
    public class SaverTestsCsv
    {
        private readonly IDataIO dataIo;
        private string path = "test.csv";

        public SaverTestsCsv()
        {
            File.Delete(path);
            dataIo = new CharacterSaverCsv(path);
        }

        [Fact]
        public void TestCharacterSaverCsv_ConstructorWorksAndCreatesFile()
        {
            dataIo.InsertOrUpdateExisting(new Thief());
            Assert.True(File.Exists(path));
        }
        [Fact]
        public void TestCharacterSaverCsv_WriteCharacterToEmptyFileCreatesHeader()
        {
            dataIo.InsertOrUpdateExisting(new Thief());
            Assert.Equal(2,File.ReadAllLines(path).Length);
        }
        [Fact]
        public void TestCharacterSaverCsv_WriteCharacterToFileCreatesMultipleObjectsWithOneHeader()
        {
            dataIo.InsertOrUpdateExisting(new Thief());
            dataIo.InsertOrUpdateExisting(new Thief());
            var chars=dataIo.Load();
            Assert.Equal(2,chars.Count);
            Assert.Equal(3,File.ReadAllLines(path).Length);
        }
        [Fact]
        public void TestCharacterSaverCsv_CheckThatThereAreNoHeadersInNewFile()
        {
            Assert.False(((CharacterSaverCsv)dataIo).HeadersExist());
        }
        [Fact]
        public void TestCharacterSaverCsv_CheckThatThereAreHeadersInUsedFile()
        {
            dataIo.InsertOrUpdateExisting(new Thief());
            Assert.True(((CharacterSaverCsv)dataIo).HeadersExist());
        }
        [Fact]
        public void TestTest()
        {
            Assert.True(true);
        }

        [Fact]
        public void testAppendingToExistingFile()
        {
            dataIo.InsertOrUpdateExisting(new Thief());
        }
    }
}
