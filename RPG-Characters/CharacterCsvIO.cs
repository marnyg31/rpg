﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class CharacterSaverCsv : IDataIO
    {
        public string SaveFilePath { get; set; }

        public CharacterSaverCsv(string path) { SaveFilePath = path; }
        public List<Character> Load()
        {
            List<Character> characters = new List<Character>();
            try
            {
                using var reader = new StreamReader(SaveFilePath);
                using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
                Character c = null;
                csv.Read();
                do
                {
                    c = csv.GetRecord<Warrior>();
                    if (((Character)c).ClassType == ClassType.Warrior) characters.Add(c);
                    else if (((Character)c).ClassType == ClassType.Wizard) characters.Add(csv.GetRecord<Wizard>());
                    else characters.Add(csv.GetRecord<Thief>());
                    csv.Read();
                } while (c != null);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return characters;
        }

        public int InsertOrUpdateExisting(Character character)
        {
            void AppendCharacterToFile(CsvWriter csv)
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.WriteRecord(character);
                csv.NextRecord();
            }

            bool headersExist = HeadersExist();
            try
            {
                using (var stream = File.Open(SaveFilePath, FileMode.Append))
                using (var writer = new StreamWriter(stream))
                using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                {
                    if (!headersExist) WriteHeadersIfTheyDintAlreadyExist(csv);
                    AppendCharacterToFile(csv);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return 0;
        }

        public void Delete(Character character)
        {
            throw new NotImplementedException();
        }

        public bool HeadersExist()
        {
            bool headersExist = true;
            try
            {
                string[] headerRow;
                using var reader = new StreamReader(SaveFilePath);
                using (var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csvReader.Read();
                    csvReader.ReadHeader();
                    headerRow = csvReader.Context.HeaderRecord;
                }
            }
            catch (ReaderException e)
            {
                headersExist = false;
            }
            catch (FileNotFoundException e)
            {
                headersExist = false;
            }

            return headersExist;
        }
        private void WriteHeadersIfTheyDintAlreadyExist(CsvWriter csv)
        {
            csv.WriteHeader<Character>();
            csv.NextRecord();
        }
    }
}
