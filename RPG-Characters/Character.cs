﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public enum ClassType { Warrior, Thief, Wizard }

    //abstract base class of Character
    public abstract class Character
    {
        public int ID { get; set; }//pk
        public int HP { get; set; }
        public int Energy { get; set; }
        public int Armor { get; set; }
        public string Name { get; set; }
        public ClassType ClassType { get; set; }


        public abstract void Attack(Character receiver);
        public abstract void Move();

        public override string ToString()
        {
            return Name;
        }
        public string Summary()
        {
            return Name + " is a " + GetType().Name + "\nHis stats are:" + "\n\nHP:" + HP + "\nEnergy: " + Energy + "\nArmor: " + Armor;
        }
    }

    //warrior subtype of Character
    public class Warrior : Character
    {
        public override void Attack(Character receiver) { }
        public override void Move() { }
    }

    //thief subtype of Character
    public class Thief : Character
    {
        public override void Attack(Character receiver) { }
        public override void Move() { }
    }

    //wizard subtype of Character
    public class Wizard : Character
    {
        public override void Attack(Character receiver) { }
        public override void Move() { }
    }
}
