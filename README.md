Solution for task 11(RPG)

Runnig the program will start a Windows Forms aplication. In this gui you can create and edit characters, and submit store these characters in a database. The application assumese that a database exists on the local pc with the name "RPG".

The database used is defined in the Form.cs file with the line:
```c#
private readonly IDataIO _dataIo = new CharacterSqlIo(@"PC7581\SQLEXPRESS","RPG");
```
In this database there should be a table named Characters, created with:
```SQLEXPRESS
CREATE TABLE [dbo].[Characters](
    [ID] [int] IDENTITY(1,1) NOT NULL,
    [Energ] [int] NULL,
    [HP] [int] NULL,
    [Aurmor] [int] NULL,
    [name] [nvarchar](50) NULL,
    [classType] [int] NULL
    ) ON [PRIMARY]
GO
```
 
the nuget packages used in this repo, that you might need to install include:
* CsvHelper
* Xunit
* SqlClient



